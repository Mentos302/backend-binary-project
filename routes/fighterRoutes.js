const { Router } = require('express')
const FighterService = require('../services/fighterService')
const { responseMiddleware } = require('../middlewares/response.middleware')
const {
  createFighterValid,
  updateFighterValid,
} = require('../middlewares/fighter.validation.middleware')

const router = Router()

router.get(
  '/',
  (req, res, next) => {
    try {
      res.body = FighterService.find()
    } catch (err) {
      res.err = err
    } finally {
      next()
    }
  },
  responseMiddleware
)

router.get(
  '/:id',
  (req, res, next) => {
    try {
      res.body = FighterService.findAll(req.params.id)
    } catch (err) {
      res.err = err
    } finally {
      next()
    }
  },
  responseMiddleware
)

router.post(
  '/',
  (req, res, next) => {
    try {
      createFighterValid(req, res, next)
      FighterService.create(req.body)
    } catch (err) {
      res.err = err
    } finally {
      next()
    }
  },
  responseMiddleware
)

router.put(
  '/:id',
  (req, res, next) => {
    try {
      updateUserValid(req, res, next)
      FighterService.update(req.params.id, req.body)
    } catch (err) {
      res.err = err
    } finally {
      next()
    }
  },
  responseMiddleware
)

router.delete(
  '/:id',
  (req, res, next) => {
    try {
      UserService.delete(req.params.id)
    } catch (err) {
      res.err = err
    } finally {
      next()
    }
  },
  responseMiddleware
)

module.exports = router
