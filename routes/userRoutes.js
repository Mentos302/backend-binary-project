const { Router } = require('express')
const UserService = require('../services/userService')
const {
  createUserValid,
  updateUserValid,
} = require('../middlewares/user.validation.middleware')
const { responseMiddleware } = require('../middlewares/response.middleware')

const router = Router()

router.get(
  '/',
  (req, res, next) => {
    try {
      res.body = UserService.searchAll()
    } catch (err) {
      res.err = err
    } finally {
      next()
    }
  },
  responseMiddleware
)

router.get(
  '/:id',
  (req, res, next) => {
    try {
      res.body = UserService.search(req.params.id)
    } catch (err) {
      res.err = err
    } finally {
      next()
    }
  },
  responseMiddleware
)

router.post(
  '/',
  async (req, res, next) => {
    try {
      createUserValid(req, res, next)
      UserService.create(req.body)
    } catch (err) {
      res.err = err
    } finally {
      next()
    }
  },
  responseMiddleware
)

router.put(
  '/:id',
  (req, res, next) => {
    try {
      updateUserValid(req, res, next)
      UserService.update(req.params.id, req.body)
    } catch (err) {
      res.err = err
    } finally {
      next()
    }
  },
  responseMiddleware
)

router.delete(
  '/:id',
  (req, res, next) => {
    try {
      UserService.delete(req.params.id)
    } catch (err) {
      res.err = err
    } finally {
      next()
    }
  },
  responseMiddleware
)

module.exports = router
