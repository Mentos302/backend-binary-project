const { fighter } = require('../models/fighter')

const createFighterValid = (req, res, next) => {
  delete fighter.id
  delete fighter.health
  delete req.body.health
  if (
    JSON.stringify(Object.keys(req.body).sort()) ===
    JSON.stringify(Object.keys(fighter).sort())
  ) {
    valuesValidation(req.body)
    res.body = req.body
  } else {
    throw {
      status: 400,
      message: `Validation failed`,
    }
  }
}

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  next()
}

exports.createFighterValid = createFighterValid
exports.updateFighterValid = updateFighterValid

const valuesValidation = (fighter) => {
  if (!fighter.power || fighter.power > 100) {
    throw {
      status: 400,
      message: `Power can not be negative or bigger than 100`,
    }
  } else if (!fighter.defense || fighter.defense > 10) {
    throw {
      status: 400,
      message: `Defense can not be negative or bigger than 10`,
    }
  }

  return true
}
