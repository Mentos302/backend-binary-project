const { user } = require('../models/user')

const createUserValid = (req, res, next) => {
  delete user.id
  if (
    JSON.stringify(Object.keys(req.body).sort()) ===
    JSON.stringify(Object.keys(user).sort())
  ) {
    valuesValidation(req.body)
    res.body = req.body
  } else {
    throw {
      status: 400,
      message: `Validation failed`,
    }
  }
}

const updateUserValid = (req, res, next) => {
  if (valuesValidation(req.body)) next()
}

exports.createUserValid = createUserValid
exports.updateUserValid = updateUserValid

const valuesValidation = (user) => {
  if (user.email.substr(user.email.length - 9, 9) !== 'gmail.com') {
    throw {
      status: 400,
      message: `Only Gmail is available to sign up`,
    }
  } else if (
    user.phoneNumber.substr(0, 4) !== `+380` ||
    user.phoneNumber.length > 13
  ) {
    throw {
      status: 400,
      message: `Phone number should start with the '+380'`,
    }
  } else if (typeof user.password !== 'string' || user.password.length < 3) {
    throw {
      status: 400,
      message: `Minimal lenght of password is 3!`,
    }
  }

  return true
}
