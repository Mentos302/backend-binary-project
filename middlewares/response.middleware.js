const responseMiddleware = (req, res, next) => {
  if (res.err) {
    res.status(res.err.status)
    res.send({
      error: true,
      message: res.err.message,
    })
  } else {
    res.status(200)
    res.send(res.body ? res.body : null)
  }
  next()
}

exports.responseMiddleware = responseMiddleware
