const { UserRepository } = require('../repositories/userRepository')

class UserService {
  create(user) {
    const user = UserRepository.getOne({ email: user.email })
    const phone = UserRepository.getOne({ phoneNumber: user.phoneNumber })

    if (user || phone) {
      try {
        UserRepository.create(user)
      } catch (error) {
        throw {
          status: 400,
          message: `Created failed.`,
        }
      }
    } else {
      throw {
        status: 400,
        message: `Please write another email or phone.`,
      }
    }
  }

  searchAll() {
    const items = UserRepository.getAll(search)

    return items
  }

  search(search) {
    const item = UserRepository.getOne(search)
    if (!item) {
      return null
    }
    return item
  }

  update(id, data) {
    UserRepository.update(id, data)
  }

  delete(id) {
    UserRepository.delete(id)
  }
}

module.exports = new UserService()
