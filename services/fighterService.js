const { FighterRepository } = require('../repositories/fighterRepository')

class FighterService {
  find() {
    const items = FighterRepository.getAll()

    return items
  }

  findOne(id) {
    const items = FighterRepository.getOne(id)

    return items
  }

  create(data) {
    const fighter = UserRepository.getOne({ name: data.name })

    if (fighter) {
      throw {
        status: 400,
        message: `Please, choose another name for your fighter`,
      }
    } else {
      try {
        FighterRepository.create(data)
      } catch (error) {
        throw {
          status: 400,
          message: `Fighter created failed.`,
        }
      }
    }
  }

  update() {}

  delete() {}
}

module.exports = new FighterService()
